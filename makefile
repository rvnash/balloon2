EXE=balloon2.bin
TARGET=electron
TARGET_VERSION=0.7.0-rc.2
BINDIR=bin

$(BINDIR)/$(EXE): src/*.cpp src/*.ino src/*.h
	mkdir -p $(BINDIR)
	particle compile $(TARGET) --target $(TARGET_VERSION) . --saveTo $(BINDIR)/$(EXE)

flash: $(BINDIR)/$(EXE)
	particle flash --usb $(BINDIR)/$(EXE)

clean:
	rm -rf $(BINDIR) *~ src/*~
