/*
TinyGPS -
Rewritten from TinyGPS by Mikal Hart
*/

#include "TinyGPS.h"
#include <math.h>

#define _GPRMC_TERM   "GPRMC"
#define _GPGGA_TERM   "GPGGA"

// Converts degrees to radians.
#define radians(angleDegrees) (angleDegrees * M_PI / 180.0f)

// Converts radians to degrees.
#define degrees(angleRadians) (angleRadians * 180.0f / M_PI)

#define TWO_PI 6.283185307179586476925286766559f
#define PI_2 (6.283185307179586476925286766559f / 4.0f)
#define sq(x) ((x)*(x))

#define ASCENT_FILTER 0.5f

// Earth's radius in meters
#define EARTH_RADIUS 6372795.0f

#define _GPS_MPH_PER_KNOT 1.15077945f
#define _GPS_MPS_PER_KNOT 0.51444444f
#define _GPS_KMPH_PER_KNOT 1.852f
#define _GPS_MILES_PER_METER 0.00062137112f
#define _GPS_KM_PER_METER 0.001f
#define _GPS_METER_PER_CM 0.01f
#define _GPS_METERS_PER_FOOT 0.3048f
#define _GPS_FEET_PER_CM ((1.0f / _GPS_METERS_PER_FOOT) * _GPS_METER_PER_CM)

TinyGPS::TinyGPS()
{
  resetFix();
}

void TinyGPS::resetFix()
{
  _term[0] = '\0';
  _time = GPS_INVALID_TIME;
  _date = GPS_INVALID_DATE;
  _latitude = GPS_INVALID_LATLONG;
  _longitude = GPS_INVALID_LATLONG;
  _altitude = GPS_INVALID_ALTITUDE;
  _speed = GPS_INVALID_F_SPEED;
  _course = GPS_INVALID_ANGLE;
  _hdop = 0;
  _numsats = 0;
  _ascent_rate = 0;
  _last_position_fix = GPS_INVALID_FIX_TIME;
  _goodFixesInARow = 0;;
}

//
// public methods
//
bool TinyGPS::encode(char c)
{
  bool valid_sentence = false;

  switch(c)
  {
  case ',': // term terminators
    _parity ^= c;
  case '\r':
  case '\n':
  case '*':
    if (_term_offset < sizeof(_term))
    {
      _term[_term_offset] = 0;
      valid_sentence = term_complete();
    }
    ++_term_number;
    _term_offset = 0;
    _is_checksum_term = c == '*';
    break;
  case '$': // sentence begin
    _term_number = _term_offset = 0;
    _parity = 0;
    _sentence_type = _GPS_SENTENCE_OTHER;
    _is_checksum_term = false;
    _gps_data_good = false;
    break;
  default:
    // ordinary characters
    if (_term_offset < sizeof(_term) - 1)
      _term[_term_offset++] = c;
    if (!_is_checksum_term)
      _parity ^= c;
    break;
  }
  if (valid_sentence) positionEncoded();
  return valid_sentence;
}

uint32_t TinyGPS::timeSinceLastPosition()
{
  return (_latitude == GPS_INVALID_LATLONG) ? GPS_INVALID_FIX_TIME : (Time.now() - _last_position_fix);
}

float TinyGPS::altitudeMeters()
{
  return _altitude == GPS_INVALID_ALTITUDE ? GPS_INVALID_F_ALTITUDE : _altitude * _GPS_METER_PER_CM;
}

float TinyGPS::altitudeFeet()
{
  return _altitude == GPS_INVALID_ALTITUDE ? GPS_INVALID_F_ALTITUDE : _altitude * _GPS_FEET_PER_CM;
}

float TinyGPS::ascentRateMps()
{
  return _ascent_rate;
}

float TinyGPS::ascentRateFps()
{
  return _ascent_rate * (1.0f / _GPS_METERS_PER_FOOT);
}

float TinyGPS::courseDegrees()
{
  return _course == GPS_INVALID_ANGLE ? GPS_INVALID_F_ANGLE : _course * (1.0f / 100.0f);
}

float TinyGPS::speedKnots()
{
  return _speed == GPS_INVALID_F_SPEED ? GPS_INVALID_F_SPEED : _speed;
}

float TinyGPS::speedMph()
{
  return _speed == GPS_INVALID_F_SPEED ? GPS_INVALID_F_SPEED : _GPS_MPH_PER_KNOT * speedKnots();
}

float TinyGPS::speedMps()
{
  return _speed == GPS_INVALID_F_SPEED ? GPS_INVALID_F_SPEED : _GPS_MPS_PER_KNOT * speedKnots();
}

float TinyGPS::speedKmph()
{
  return _speed == GPS_INVALID_F_SPEED ? GPS_INVALID_F_SPEED : _GPS_KMPH_PER_KNOT * speedKnots();
}

// 3 good datapoints in a row and a fix within last 5 seconds
bool TinyGPS::haveValidGPSFix()
{
  return (_goodFixesInARow >= 3) && (timeSinceLastPosition() <= 5);
}

bool TinyGPS::isNearGround() // Less than 5000 feet
{
  return altitudeFeet() < 5000.0f;
}

bool TinyGPS::isMoving() // Faster than "walking" pace
{
  // Walking speed tends to be about 3mph, so lets go with 2.5pph
  return speedMph() > 2.5f;
}

void TinyGPS::getDateTime(int32_t *year, uint8_t *month, uint8_t *day,
  uint8_t *hour, uint8_t *minute, uint8_t *second, uint8_t *hundredths, uint32_t *age)
{
  if (year)
  {
    *year = _date % 100;
    *year += *year > 80 ? 1900 : 2000;
  }
  if (month) *month = (_date / 100) % 100;
  if (day) *day = _date / 10000;
  if (hour) *hour = _time / 1000000;
  if (minute) *minute = (_time / 10000) % 100;
  if (second) *second = (_time / 100) % 100;
  if (hundredths) *hundredths = _time % 100;
}

//
// static utilities
//
float TinyGPS::distanceBetweenMeters(float lat1, float long1, float lat2, float long2)
{
  // returns distance in meters between two positions, both specified
  // as signed decimal-degrees latitude and longitude. Uses great-circle
  // distance computation for hypothetical sphere of radius 6372795 meters.
  // Because Earth is no exact sphere, rounding errors may be up to 0.5%.
  // Rewritten by rvnash
  float l1 = radians(lat1);
  float l2 = radians(lat2);
  float sinDeltaLat = sin((l2-l1)/2.0f);
  float sinDeltaLon = sin((radians(long2)-radians(long1))/2.0f);

  float a = sinDeltaLat * sinDeltaLat +
            cos(l1) * cos(l2) *
            sinDeltaLon * sinDeltaLon;
  float c = 2.0f * atan2(sqrt(a), sqrt(1.0f-a));

  float d = EARTH_RADIUS * c;
  return d;
}

float TinyGPS::distanceBetweenFeet(float lat1, float long1, float lat2, float long2)
{
  return distanceBetweenMeters(lat1,long1,lat2,long2) * (1.0f / _GPS_METERS_PER_FOOT);
}

float TinyGPS::course_to (float lat1, float long1, float lat2, float long2)
{
  // returns course in degrees (North=0, West=270) from position 1 to position 2,
  // both specified as signed decimal-degrees latitude and longitude.
  // Because Earth is no exact sphere, calculated course may be off by a tiny fraction.
  // Courtesy of Maarten Lamers
  long1 = radians(long1);
  lat1 = radians(lat1);
  long2 = radians(long2);
  lat2 = radians(lat2);
  float y = sin(long2-long1) * cos(lat2);
  float x = cos(lat1)*sin(lat2) -
          sin(lat1)*cos(lat2)*cos(long2-long1);
  float brng = atan2(y, x);
  if (brng < 0.0)
  {
    brng += TWO_PI;
  }
  return degrees(brng);
}

// Great site for lat/log computation info
// http://www.movable-type.co.uk/scripts/latlong.html
void TinyGPS::getDestPoint(float lat1, float long1, float course, float dist, float *outLat, float *outLong)
{
  float brng = radians(course); // Clockwise from north
  if (brng < 0.0) brng += TWO_PI;
  float rlat1 = radians(lat1);
  float rlong1 = radians(long1);
  float rlat2 = asin( sin(rlat1) * cos(dist/EARTH_RADIUS) +
                   cos(rlat1) * sin(dist/EARTH_RADIUS) * cos(brng) );
  float rlong2 = rlong1 + atan2(sin(brng) * sin(dist/EARTH_RADIUS) * cos(rlat1),
                               cos(dist/EARTH_RADIUS) - sin(rlat1) * sin(rlat2));
  *outLat = degrees(rlat2);
  *outLong = degrees(rlong2);
}

const char *TinyGPS::cardinal (float course)
{
  static const char* directions[] = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"};

  int32_t direction = (int32_t)((course + 11.25f) / 22.5f);
  return directions[direction % 16];
}

//
// internal utilities
//
void TinyGPS::positionEncoded()
{
  if (_date == TinyGPS::GPS_INVALID_DATE || _time == TinyGPS::GPS_INVALID_TIME) {
    _goodFixesInARow = 0;
  } else {
    _goodFixesInARow++;
  }
}

int32_t TinyGPS::from_hex(char a)
{
  if (a >= 'A' && a <= 'F')
    return a - 'A' + 10;
  else if (a >= 'a' && a <= 'f')
    return a - 'a' + 10;
  else
    return a - '0';
}

uint32_t TinyGPS::parse_decimal()
{
  char *p = _term;
  bool isneg = *p == '-';
  if (isneg) ++p;
  uint32_t ret = 100UL * gpsatol(p);
  while (gpsisdigit(*p)) ++p;
  if (*p == '.')
  {
    if (gpsisdigit(p[1]))
    {
      ret += 10 * (p[1] - '0');
      if (gpsisdigit(p[2]))
        ret += p[2] - '0';
    }
  }
  return isneg ? -ret : ret;
}

// Parse a string in the form ddmm.mmmmmmm...
float TinyGPS::parse_degrees()
{
  char *p;
  uint32_t left_of_decimal = gpsatol(_term);
  uint32_t hundred1000ths_of_minute = (left_of_decimal % 100UL) * 100000UL;
  for (p=_term; gpsisdigit(*p); ++p);
  if (*p == '.')
  {
    uint32_t mult = 10000;
    while (gpsisdigit(*++p))
    {
      hundred1000ths_of_minute += mult * (*p - '0');
      mult /= 10;
    }
  }
  uint32_t mdegrees = (left_of_decimal / 100) * 1000000 + (hundred1000ths_of_minute + 3) / 6;
  return mdegrees * (1.0f / 1000000.0f);
}

#define COMBINE(sentence_type, term_number) (((unsigned)(sentence_type) << 5) | term_number)

// Processes a just-completed term
// Returns true if new sentence has just passed checksum test and is validated
bool TinyGPS::term_complete()
{
  if (_is_checksum_term)
  {
    uint8_t checksum = 16 * from_hex(_term[0]) + from_hex(_term[1]);
    if (checksum == _parity)
    {
      if (_gps_data_good)
      {
        switch(_sentence_type)
        {
        case _GPS_SENTENCE_GPRMC:
          _time      = _new_time;
          _date      = _new_date;
          _latitude  = _new_latitude;
          _longitude = _new_longitude;
          _speed     = _new_speed;
          _course    = _new_course;
          break;
        case _GPS_SENTENCE_GPGGA:
          if (_altitude != GPS_INVALID_ALTITUDE) {
            uint32_t now = millis();
            float currentAscentRate = ((_new_altitude - _altitude) * _GPS_METER_PER_CM) / ((float)(now - _last_altitude_millis) * (1.0f/1000.0f));
            _ascent_rate = (ASCENT_FILTER * _ascent_rate) + ((1.0f-ASCENT_FILTER) * currentAscentRate);
            _last_altitude_millis = now;
          }
          _altitude  = _new_altitude;
          _time      = _new_time;
          _latitude  = _new_latitude;
          _longitude = _new_longitude;
          _numsats   = _new_numsats;
          _hdop      = _new_hdop;
          break;
        }
        _last_position_fix = _new_position_fix;
        return true;
      }
    }
    return false;
  }

  // the first term determines the sentence type
  if (_term_number == 0)
  {
    if (!gpsstrcmp(_term, _GPRMC_TERM))
      _sentence_type = _GPS_SENTENCE_GPRMC;
    else if (!gpsstrcmp(_term, _GPGGA_TERM))
      _sentence_type = _GPS_SENTENCE_GPGGA;
    else
      _sentence_type = _GPS_SENTENCE_OTHER;
    return false;
  }

  if (_sentence_type != _GPS_SENTENCE_OTHER && _term[0])
    switch(COMBINE(_sentence_type, _term_number))
  {
    case COMBINE(_GPS_SENTENCE_GPRMC, 1): // Time in both sentences
    case COMBINE(_GPS_SENTENCE_GPGGA, 1):
      _new_time = parse_decimal();
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 2): // GPRMC validity
      _gps_data_good = _term[0] == 'A';
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 3): // Latitude
    case COMBINE(_GPS_SENTENCE_GPGGA, 2):
      _new_latitude = parse_degrees();
      _new_position_fix = Time.now();
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 4): // N/S
    case COMBINE(_GPS_SENTENCE_GPGGA, 3):
      if (_term[0] == 'S')
        _new_latitude = -_new_latitude;
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 5): // longitude
    case COMBINE(_GPS_SENTENCE_GPGGA, 4):
      _new_longitude = parse_degrees();
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 6): // E/W
    case COMBINE(_GPS_SENTENCE_GPGGA, 5):
      if (_term[0] == 'W')
        _new_longitude = -_new_longitude;
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 7): // Speed (GPRMC)
      _new_speed = (float)parse_decimal() / 100.0f;
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 8): // Course (GPRMC)
      _new_course = parse_decimal();
      break;
    case COMBINE(_GPS_SENTENCE_GPRMC, 9): // Date (GPRMC)
      _new_date = gpsatol(_term);
      break;
    case COMBINE(_GPS_SENTENCE_GPGGA, 6): // Fix data (GPGGA)
      _gps_data_good = _term[0] > '0';
      break;
    case COMBINE(_GPS_SENTENCE_GPGGA, 7): // Satellites used (GPGGA)
      _new_numsats = (unsigned char)atoi(_term);
      break;
    case COMBINE(_GPS_SENTENCE_GPGGA, 8): // HDOP
      _new_hdop = parse_decimal();
      break;
    case COMBINE(_GPS_SENTENCE_GPGGA, 9): // Altitude (GPGGA)
      _new_altitude = parse_decimal();
      break;
  }
  return false;
}

int32_t TinyGPS::gpsatol(const char *str)
{
  int32_t ret = 0;
  while (gpsisdigit(*str))
    ret = 10 * ret + *str++ - '0';
  return ret;
}

int32_t TinyGPS::gpsstrcmp(const char *str1, const char *str2)
{
  while (*str1 && *str1 == *str2)
    ++str1, ++str2;
  return *str1;
}
