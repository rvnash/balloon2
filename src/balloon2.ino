/*
balloon2 -
Copyright (C) 2016 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "balloon2.h"
#include "APRS.h"
#include "CellConnector.h"
#include "TinyGPS.h"
#include "SdFat.h"
#include <InLoopPeriodicCallback.h>
#include <ThingSpeak.h>
#include <Adafruit_BMP085.h>

SYSTEM_MODE(MANUAL);
SYSTEM_THREAD(ENABLED);

// How long to wait for a cellular connection
#define CONNECT_TIMEOUT (3UL * 60UL * 1000UL)

// Concern that some things like waiting for cellular reconnection might trigger reset
#define WATCHDOG_TIMEOUT (60UL * 1000UL)
ApplicationWatchdog wd(WATCHDOG_TIMEOUT, System.reset);

#define BALLOON_VERSION "0.2"

// Recomendations from APRS are 30 seconds for mobile, and 1 minute while stationary
// Doing 1minute while mobile, 3 while stationary
#define APRS_MOBILE_PERIOD  (uint32_t)(30UL)
#define APRS_STATIONARY_PERIOD  (uint32_t)(60UL)

// Options
const bool send_ThingSpeak = false;

// GPS and positional information
TinyGPS gps;
CellConnector cell;

// APRS
APRS aprs(APRS_DAC_PIN, APRS_PTT_PIN, APRS_PREAMBLE_FLAG_COUNTS, APRS_POSTAMBLE_FLAG_COUNTS);

// For ThingSpeak
TCPClient client;
unsigned long channelID= 313390;
const char * writeAPIKey = "HZW747KS182G9KW2";

// Last Sent information
uint32_t timeOfLastSend=0;
float latitudeLastSent=-1.0f, longitudeLastSent=-1.0f;
char serialCommand = 0;

// Battery status
FuelGauge fuel;

// First fix sets clock and starts logging
bool fInitializedClockAndStartLogging;
char logFileName[30];

// SD Card information
#define SCK A3
#define  MISO A4
#define MOSI A5
#define  SS A2
SdFat sd;
const uint8_t chipSelect = SS;
File myFile;

// Temp sensor
Adafruit_BMP085 bmp;
boolean fHasBMP;

// Packet counter
int sequenceCount = 1;

class PeriodicBlink: public LEDStatus {
public:
    explicit PeriodicBlink(LEDPriority priority,
                          uint32_t color1,
                          uint32_t time1,
                          uint32_t color2,
                          uint32_t time2) :
        LEDStatus(LED_PATTERN_CUSTOM, priority) {
          this->colorTicks = 0;
          this->color1 = color1;
          this->time1 = time1;
          this->color2 = color2;
          this->time2 = time2;
    }
protected:
    virtual void update(system_tick_t ticks) override {
        // Change status color every 300 milliseconds
        colorTicks += ticks;
        if (colorTicks >= time1 + time2) {
            setColor(color1);
            colorTicks -= time1 + time2;
        } else if (colorTicks >= time1) {
            setColor(color2);
        } else {
            setColor(color1);
        }
    }

private:
    system_tick_t colorTicks;
    uint32_t color1, color2, time1, time2;
};

PeriodicBlink blinkGPSFix(LED_PRIORITY_NORMAL, RGB_COLOR_GREEN, 10, 0x0, 2000);
PeriodicBlink blinkNoGPSFix(LED_PRIORITY_NORMAL, RGB_COLOR_RED, 200, 0x0, 200);
PeriodicBlink blinkSendRF(LED_PRIORITY_NORMAL, RGB_COLOR_ORANGE, 100, 0x0, 100);
PeriodicBlink blinkSendIS(LED_PRIORITY_NORMAL, RGB_COLOR_YELLOW, 100, 0x0, 100);
PeriodicBlink blinkConnectingCellular(LED_PRIORITY_NORMAL, RGB_COLOR_CYAN, 100, 0x0, 100);
PeriodicBlink *activeBlink;

// Periodic Status Print
bool printStatus(unsigned long);
InLoopPeriodicCallback cbPrintStatus(printStatus, 3000UL, 3000UL, 3000UL, true);
bool logStatus(unsigned long);
InLoopPeriodicCallback cbLogStatus(logStatus, 60000UL, 60000UL, 60000UL, true);

void setup()
{
  pinMode( D1, INPUT_PULLDOWN ); // Not used, except by System.sleep
  pinMode( APRS_IS_ENABLED, INPUT_PULLUP ); // APRS Enabled
  Serial.begin(9600);       // USB
  Serial1.begin(9600);       // Pins (RX/TX), GPS is connected here
  blinkNoGPSFix.setActive();
  activeBlink = &blinkNoGPSFix;
  delay(1000);
  String version = System.version();
  Serial.printf("Starting Tracker\nSystem Version: %s\nSoftware Version %s\n"
                "Send TS: %s\n",
                version.c_str(), BALLOON_VERSION,
                send_ThingSpeak ? "true" : "false");
  cbPrintStatus.init();
  ThingSpeak.begin(client);
  // Change to SPI_FULL_SPEED for more performance.
  if (!sd.begin(chipSelect, SPI_HALF_SPEED)) {
    Serial.printf("SD Card not present or not functioning.\n");
  }
  if (!bmp.begin()) {
     Serial.println("Could not find a valid BMP sensor.");
     fHasBMP = false;
   } else {
     fHasBMP = true;
   }
  fInitializedClockAndStartLogging = false;
}

// Character ready on Serial Port 1
void serialEvent1()
{
  while (Serial1.available()) gps.encode(Serial1.read());
}


char *getUTCTimeString()
{
  int32_t year;
  uint8_t month, day, hour, minute, second, hundredths;
  uint32_t fix_age;
  static char str[30];

  gps.getDateTime(&year, &month, &day, &hour, &minute, &second, &hundredths, &fix_age);
  sprintf( str, "%04d-%02d-%02d %02d:%02d:%02d", (int)year, (int)month, (int)day, (int)hour, (int)minute, (int)second);
  return str;
}

void logLine(const char *comment)
{
  if (!myFile.open(logFileName, O_RDWR | O_AT_END)) {
    // Need to create the file.
    if (!myFile.open(logFileName, O_RDWR | O_CREAT |O_AT_END)) {
      Serial.printf("Can't log\n");
      return;
    }
    Serial.printf("File created");
    myFile.printf("Timestamp,Altitude,Latitude,Longitude,Ascent Rate,Bering,Speed,Temperature,Pressure,Humidity,Pressure Altitude,Comment\n");
  }
  bool fix = gps.haveValidGPSFix();


  myFile.printf("%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%s\n",
                 getUTCTimeString(),
                 fix ? gps.altitudeFeet() : 0.0f, // Alt
                 fix ? gps.latitude() : 0.0f, fix ? gps.longitude() : 0.0f, // Lat, long
                 fix ? gps.ascentRateFps() : 0.0f, // Ascent Rate
                 fix ? (float)gps.courseDegrees() : 0.0f, // Bering
                 fix ? (float)gps.speedMph() : 0.0f, // Speed
                 fHasBMP ? bmp.readTemperature() : 0.0f, // Temp
                 fHasBMP ? bmp.readPressure() : 0.0f, // Pressure
                 0.0, // Humidity
                 fHasBMP ? bmp.readAltitude() : 0.0f,
                 comment
               );
  myFile.close();
}

bool logStatus(unsigned long now)
{
  logLine("status");
  return true;
}

bool printStatus(unsigned long now)
{
  float latitude, longitude;
  uint32_t since, minutesSinceLastSend, secsSinceLastSend;

  latitude = gps.latitude();
  longitude = gps.longitude();
  since = gps.timeSinceLastPosition();
  minutesSinceLastSend = since/60;
  secsSinceLastSend = since - minutesSinceLastSend*60;

  if (gps.haveValidGPSFix()) {
    float dist = TinyGPS::distanceBetweenFeet(latitude, longitude, latitudeLastSent, longitudeLastSent);

    Serial.printf("pos: %f, %f, age: %03d:%02d, dist: %.1f, alt: %.2f, rate: %.1f, head: %0.0f, speed: %0.0f",
                  latitude, longitude,
                  minutesSinceLastSend, secsSinceLastSend,
                  dist,
                  gps.altitudeFeet(),
                  gps.ascentRateFps(),
                  gps.courseDegrees(),
                  gps.speedMph());
  } else {
    Serial.printf("No fix:");
  }
  Serial.printf(" bat: %.1f, mem: %d, IS: %s\n", fuel.getSoC(), System.freeMemory(), digitalRead(APRS_IS_ENABLED) ? "N" : "Y");
  return true;
}

String getCommentField()
{
  String s = String::format("AR=%.1f/T=%.1f/P=%.2f/SOC=%.1f/N=%d",
      gps.ascentRateFps(),
      fHasBMP ? bmp.readTemperature() : 0.0f, // Temp
      fHasBMP ? bmp.readPressure() : 0.0f, // Pressure
      fuel.getSoC(),
      sequenceCount++);
  return s;
}

void sendAPRS_IS(uint32_t connectTime, int failureCount)
{
  blinkSendIS.setActive();
  activeBlink = &blinkSendIS;

  PathAddress paths[] = {
    {(char *)APRS_D_CALLSIGN, APRS_D_CALLSIGN_ID},  // Destination callsign
    {(char *)APRS_RF_STATION, APRS_RF_STATION_ID}  // Source callsign
  };
  float latitude = gps.latitude();
  float longitude = gps.longitude();
  float altitudeFeet = gps.altitudeFeet();
  int courseDegrees = gps.courseDegrees();
  int speedKnots = gps.speedKnots();
  uint32_t timeage;
  int32_t year;
  uint8_t month, day, hour, minute, second, hundredths;
  gps.getDateTime(&year, &month, &day, &hour, &minute, &second, &hundredths, &timeage);
  String comment = getCommentField();
  Serial.println("Send APRS IS:");
  printStatus(Time.now());
  if (digitalRead(APRS_IS_ENABLED) == 0) {
    uint32_t result =
      aprs.IS_send(
        paths, 2,
        APRS_IS_SERVER, APRS_IS_PORT, APRS_IS_PASSWORD,
        day, hour, minute,
        latitude, longitude, altitudeFeet,
        courseDegrees, speedKnots,
        // Primary Table Symbols: /O=balloon, /-=House, /v=Blue Van, />=Red Car
        SYMBOL_TABLE,
        SYMBOL_ICON,
        comment.c_str());
    if (result == APRS_STATUS_OK) {
      Serial.printf("Successful\n", result);
      timeOfLastSend = Time.now();
      latitudeLastSent = latitude;
      longitudeLastSent = longitude;
    } else {
      Serial.printf("Failed APRS_IS, error code %d\n", result);
    }
  }
  if (send_ThingSpeak) {
    Serial.println("Send ThingSpeak:");
    printStatus(Time.now());
    ThingSpeak.setField(1, latitude);
    ThingSpeak.setField(2, longitude);
    ThingSpeak.setField(3, altitudeFeet);
    ThingSpeak.setField(4, courseDegrees);
    ThingSpeak.setField(5, gps.speedMph());
    ThingSpeak.setField(6, gps.ascentRateFps());
    ThingSpeak.setField(7,comment.c_str());
    ThingSpeak.writeFields(channelID, writeAPIKey);
    timeOfLastSend = Time.now();
    latitudeLastSent = latitude;
    longitudeLastSent = longitude;
  }
  blinkGPSFix.setActive();
  activeBlink = &blinkSendIS;
}

// Wrapped up all TCP/IP activity for this send.
void finishedConnection(uint32_t totalTime, bool failed)
{
  Serial.printf("Connection ended in %ul, Success: %s\n", totalTime, failed ? "false" : "true");
}

// Cellular connection failed
void failedToConnect(uint32_t connectTime, int failureCount)
{
  if (gps.isMoving()) {
    // If mobile, retry in 1 minute
    timeOfLastSend = Time.now() - (APRS_MOBILE_PERIOD - 60UL);
  } else {
    // If stationary, retry in 3 minutes
    timeOfLastSend = Time.now() - (APRS_STATIONARY_PERIOD - (3UL * 60UL));
  }
}

// Send APRS over the RF signal
// FOrmat is a little different that the IS version. Need an array
// of paths, starting with destination, source, and hops allowed
bool sendAPRS_RF()
{
  PathAddress paths[] = {
    {(char *)APRS_D_CALLSIGN, APRS_D_CALLSIGN_ID},  // Destination callsign
    {(char *)APRS_RF_STATION, APRS_RF_STATION_ID},  // Source callsign
    {(char *)NULL, 0}, // Digi1 (first digi in the chain)
    {(char *)NULL, 0}  // Digi2 (second digi in the chain)
  };
  int nPaths;

  // If above 5000 feet switch to a single hop path
  if (gps.isNearGround()) {
    // Path is "WIDE1-1,WIDE2-2"
    nPaths = 4;
    paths[2].callsign = "WIDE1";
    paths[2].ssid = 1;
    paths[3].callsign = "WIDE2";
    paths[3].ssid = 1;
  } else {
    // Path is "WIDE2-1" while aloft
    nPaths = 3;
    paths[2].callsign = "WIDE2";
    paths[2].ssid = 1;
  }

  float latitude = gps.latitude();
  float longitude = gps.longitude();
  float altitudeFeet = gps.altitudeFeet();
  int courseDegrees = gps.courseDegrees();
  int speedKnots = gps.speedKnots();
  uint32_t timeage;
  int32_t year;
  uint8_t month, day, hour, minute, second, hundredths;
  gps.getDateTime(&year, &month, &day, &hour, &minute, &second, &hundredths, &timeage);
  String comment = getCommentField();
  printStatus(Time.now());
  uint32_t result = aprs.RF_send(
               paths, nPaths,
               day, hour, minute,
               latitude, longitude, altitudeFeet,
               courseDegrees, speedKnots,
               SYMBOL_TABLE,
               SYMBOL_ICON,
               comment.c_str(), 0.0f);
  if (result == APRS_STATUS_OK) {
    timeOfLastSend = Time.now();
    latitudeLastSent = gps.latitude();
    longitudeLastSent = gps.longitude();
    return true;
  } else {
    Serial.printf("Failed APRS_RF, error code %d\n", result);
    failedToConnect(0,0);
    return false;
  }
}

void checkSendPosition()
{
  if (latitudeLastSent != -1.0f) {
    uint32_t timeSinceLastSend = Time.now() - timeOfLastSend;
    if (timeSinceLastSend < APRS_MOBILE_PERIOD) return; // If less than min time, just return;
    float dist = TinyGPS::distanceBetweenFeet(gps.latitude(), gps.longitude(), latitudeLastSent, longitudeLastSent);
    if (dist < 100.0f) {
      // If distance slince last send is less than 100 feet, then wait longer
      if (timeSinceLastSend < APRS_STATIONARY_PERIOD) {
        return;
      }
    }
  }

  // Send RF
  blinkSendRF.setActive();
  activeBlink = &blinkSendRF;
  sendAPRS_RF();
  blinkGPSFix.setActive();
  activeBlink = &blinkGPSFix;

  if ((digitalRead(APRS_IS_ENABLED) == 0) || send_ThingSpeak) {
    // Cellular is already connecting
    if ((cell.getState() == CellConnector::STATE_WAIT) && gps.isNearGround()) {
      cell.connect(CONNECT_TIMEOUT, sendAPRS_IS, failedToConnect, finishedConnection);
    }
  }

  if (cell.getState() == CellConnector::STATE_WAIT) { // If not connecting
    return;
  }
}

// Input from the console
void serialEvent()
{
  if (Serial.available()) {
    serialCommand = Serial.read();
  }
}

// If console characters are recieved, process here.
void checkCommand()
{
  bool success;
  if (serialCommand == 0) return;
  switch (serialCommand) {
    case 's': // Send RF whatever we have right now
      if (!gps.haveValidGPSFix()) {
        Serial.printf("No FIX\n");
      } else {
        Serial.printf("Sending APRS RF\n" );
        printStatus(Time.now());
        success = sendAPRS_RF();
        Serial.printf("RF Sent %s\n", success ? "true" : "false" );
      }
      break;
    case 'p': // Toggle ptt
      if (digitalRead(APRS_PTT_PIN)) {
        Serial.printf("PTT LOW\n");
        digitalWrite(APRS_PTT_PIN, LOW);
      } else {
        Serial.printf("PTT HIGH\n");
        digitalWrite(APRS_PTT_PIN, HIGH);
      }
      break;
    default:
      Serial.printf("Unknown command: '%c'\n", serialCommand);
      break;
  }
  serialCommand = 0;
}

void InitializeClockAndStartLogging()
{
  int32_t year;
  uint8_t month, day, hour, minute, second, hundredths;
  uint32_t fix_age;

  struct tm t;
  gps.getDateTime(&year, &month, &day, &hour, &minute, &second, &hundredths, &fix_age);
  t.tm_year = year-1900;
  t.tm_mon = month - 1;
  t.tm_mday = day;
  t.tm_hour = hour;
  t.tm_min = minute;
  t.tm_sec = second;
  t.tm_isdst = 0;  // not used
  time_t unixTime = mktime(&t);
  Time.setTime(unixTime);
  cbPrintStatus.init(); // Need to re-init
  cbLogStatus.init();

  sprintf(logFileName, "%04d-%02d-%02d.csv", (int)year, (int)month, (int)day);
  Serial.print("Log file name: ");
  Serial.println(logFileName);
  logLine("STARTUP");
  fInitializedClockAndStartLogging = true;
}

void loop()
{
  wd.checkin(); // Checkin the watchdog
  checkCommand(); // Parse any terminal input
  cell.loop();    // Pump the cell connect state machine
  cbPrintStatus.loop();  // Check if it is time to print to the terminal
  cbLogStatus.loop();    // Check if it is time to log
  if (gps.haveValidGPSFix()) {
    // On first fix initialize the logging and real time clock to GPS time
    //if (!fInitializedClockAndStartLogging) InitializeClockAndStartLogging();
    if (cell.getState() == CellConnector::STATE_WAIT) {
       if (activeBlink != &blinkGPSFix) {
         blinkGPSFix.setActive();
         activeBlink = &blinkGPSFix;
       }
    } else {
       if (activeBlink != &blinkConnectingCellular) {
         blinkConnectingCellular.setActive();
         activeBlink = &blinkConnectingCellular;
       }
    }
    checkSendPosition(); // Send position if it is time
  } else {
    // No GPS fix, blink red.
    if (activeBlink != &blinkNoGPSFix) {
      blinkNoGPSFix.setActive();
      activeBlink = &blinkNoGPSFix;
    }
  }
}
