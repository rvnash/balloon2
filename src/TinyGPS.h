/*
TinyGPS -
Rewritten from TinyGPS by Mikal Hart
*/

#ifndef TinyGPS_h
#define TinyGPS_h

#include "application.h"
#include <stdlib.h>

class TinyGPS
{
public:
  TinyGPS();
  void resetFix();

  bool encode(char c); // process one character received from GPS

  // Getters
  inline float latitude() { return _latitude; }
  inline float longitude() { return _longitude; }

  uint32_t timeSinceLastPosition();

  // signed altitude in centimeters (from GPGGA sentence)
  inline int32_t altitude() { return _altitude; }
  float altitudeMeters();
  float altitudeFeet();

  float ascentRateMps();
  float ascentRateFps();

  // course in last full GPRMC sentence in 100th of a degree
  inline uint32_t course() { return _course; }
  float courseDegrees();

  // speed in last full GPRMC sentence
  float speedKnots();
  float speedMph();
  float speedMps();
  float speedKmph();

  // satellites used in last full GPGGA sentence
  inline unsigned short satellites() { return _numsats; }

  // horizontal dilution of precision in 100ths
  inline uint32_t hdop() { return _hdop; }

  bool haveValidGPSFix(); // 3 good datapoints in a row and a fix within last 5 seconds
  bool isNearGround(); // Less than 5000 feet
  bool isMoving(); // Faster than "walking" pace
  void getDateTime(int32_t *year, uint8_t *month, uint8_t *day,
                      uint8_t *hour, uint8_t *minute, uint8_t *second,
                      uint8_t *hundredths = 0, uint32_t *fix_age = 0);

  // static utilities
  static float distanceBetweenMeters(float lat1, float long1, float lat2, float long2);
  static float distanceBetweenFeet(float lat1, float long1, float lat2, float long2);
  static float course_to (float lat1, float long1, float lat2, float long2);
  static void getDestPoint(float latitude, float longitude, float course, float dist, float *outLat, float *outLong);
  static const char *cardinal(float course);

  // static constants
  static const uint32_t GPS_INVALID_ANGLE = 999999999;
  static const int32_t GPS_INVALID_ALTITUDE = 999999999;
  static constexpr float GPS_INVALID_LATLONG = 1000000.0f;
  static const uint32_t GPS_INVALID_DATE = 0;
  static const uint32_t GPS_INVALID_TIME = 0xFFFFFFFF;
    static const uint32_t GPS_INVALID_FIX_TIME = 0xFFFFFFFF;
  static constexpr float GPS_INVALID_F_ANGLE = 1000.0f;
  static constexpr float GPS_INVALID_F_ALTITUDE = 1000000.0f;
  static constexpr float GPS_INVALID_F_SPEED = -1.0f;

private:
  enum {_GPS_SENTENCE_GPGGA, _GPS_SENTENCE_GPRMC, _GPS_SENTENCE_OTHER};

  // properties
  uint32_t _time, _new_time;
  uint32_t _date, _new_date;
  float _latitude, _new_latitude;
  float _longitude, _new_longitude;
  int32_t _altitude, _new_altitude;
  float  _speed, _new_speed;
  uint32_t  _course, _new_course;
  uint32_t  _hdop, _new_hdop;
  unsigned short _numsats, _new_numsats;
  uint32_t _last_altitude_millis;
  uint32_t _last_position_fix, _new_position_fix;
  float _ascent_rate;

  uint32_t _goodFixesInARow;

  // parsing state variables
  uint8_t _parity;
  bool _is_checksum_term;
  char _term[15];
  uint8_t _sentence_type;
  uint8_t _term_number;
  uint8_t _term_offset;
  bool _gps_data_good;

  // internal utilities
  void positionEncoded();
  int32_t from_hex(char a);
  uint32_t parse_decimal();
  float parse_degrees();
  bool term_complete();
  bool gpsisdigit(char c) { return c >= '0' && c <= '9'; }
  int32_t gpsatol(const char *str);
  int32_t gpsstrcmp(const char *str1, const char *str2);
};

#endif
