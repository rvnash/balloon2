/*
balloon2 -
Copyright (C) 2016 Richard Nash
All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef Balloon2_h
#define Balloon2_h

// Destination callsign: APRS (with SSID=0) is okay.
// See: http://www.aprs.org/aprs11/tocalls.txt
#define APRS_D_CALLSIGN      "APZRVN"
#define APRS_D_CALLSIGN_ID   0

// This APRS-IS server works well
#define APRS_IS_SERVER  "rotate.aprs2.net"
#define APRS_IS_PORT  8080
#define APRS_IS_PASSWORD "22969"

#define APRS_STATION "KC3ARY-1"
#define SYMBOL_TABLE '/'
#define SYMBOL_ICON 'v'

#define APRS_RF_STATION "KC3ARY"
#define APRS_RF_STATION_ID 1
#define APRS_PREAMBLE_FLAG_COUNTS 15  // About 6.7 ms per count
#define APRS_POSTAMBLE_FLAG_COUNTS 5
#define APRS_DAC_PIN DAC1
#define APRS_PTT_PIN C0
#define APRS_IS_ENABLED C1  // 0 == Send VIA IS

#endif
