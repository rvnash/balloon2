#include "CellConnector.h"

CellConnector::CellConnector()
{
  Cellular.off();
  state = STATE_WAIT;
  status = STATUS_UNKNOWN;
  lastStateTime = 0;
  failureCount = 0;
}

void CellConnector::connect(uint32_t timeout, void (*success)(uint32_t,int), void (*failure)(uint32_t,int), void (*finished)(uint32_t,bool))
{
  connectTimeout = timeout;
  successCallback = success;
  failureCalback = failure;
  finishedCallback = finished;
  state = STATE_START;
  Serial.println("Cellular: STATE_START");
  status = STATUS_UNKNOWN;
  startConnectTime = lastStateTime = millis();
}

int CellConnector::getState()
{
  return state;
}

int CellConnector::getStatus()
{
  return status;
}

void CellConnector::abort()
{
  if (state != STATE_START) {
    state = STATE_DISCONNECT;
    status = STATUS_FAILED_CONNECT;
    lastStateTime = millis();
    Serial.println("Cellular: STATE_DISCONNECT");
  }
}

void CellConnector::loop()
{
  switch(state) {
    case STATE_START:
      // Serial.println("connecting...");
      Cellular.on();
      Cellular.connect();
      state = STATE_CONNECTING;
      status = STATUS_UNKNOWN;
      Serial.println("Cellular: STATE_CONNECTING");
      break;

    case STATE_CONNECTING:
      if (Cellular.ready()) {
        // Cellular connection established and have an IP address
        state = STATE_READY;
        lastStateTime = millis();
        Serial.println("Cellular: STATE_READY");
      } else
        if (Cellular.connecting()) {
          if (millis() - startConnectTime >= connectTimeout) {
            Serial.printf("Cellular: Timeout connecting after: %lu\n", millis() - startConnectTime);
            state = STATE_DISCONNECT;
            status = STATUS_FAILED_CONNECT_TIMEOUT;
            lastStateTime = millis();
            Serial.println("Cellular: STATE_DISCONNECT");
          }
        } else
          if (Cellular.listening()) {
            // This usually happens if you have no SIM card
            Serial.println("Cellular: Listening mode");
            state = STATE_DISCONNECT;
            status = STATUS_FAILED_CONNECT;
            lastStateTime = millis();
            Serial.println("Cellular: STATE_DISCONNECT");
          }
      break;

    case STATE_READY:
      state = STATE_DO_TCP;
      status = STATUS_SUCCESS;
      lastStateTime = millis();
      Serial.println("Cellular: STATE_DO_TCP");
      break;

    case STATE_DO_TCP:
      // In this state, the cellular connection is up, you can do IP stuff here
      if (successCallback) {
        (*successCallback)(millis()-startConnectTime,failureCount);
        failureCount = 0;
        state = STATE_DISCONNECT;
        lastStateTime = millis();
        Serial.println("Cellular: STATE_DISCONNECT");
      }
      break;

    case STATE_DISCONNECT:
      Cellular.disconnect();
      Cellular.off();
      if (status != STATUS_SUCCESS && failureCalback) {
        failureCount++;
        (*failureCalback)(millis()-startConnectTime,failureCount);
      }
      state = STATE_WAIT;
      lastStateTime = millis();
      Serial.println("Cellular: STATE_WAIT");
      if (finishedCallback) {
        finishedCallback(millis()-startConnectTime, (status != STATUS_SUCCESS));
      }
      break;

    case STATE_WAIT:
      break;
  }
}
