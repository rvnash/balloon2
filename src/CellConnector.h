#include "Particle.h"

class CellConnector
{
public:
  // Finite state machine states
  enum { // Values returned by getState
    STATE_START,
    STATE_CONNECTING,
    STATE_READY,
    STATE_DO_TCP,
    STATE_DISCONNECT,
    STATE_WAIT
  };

  enum { // Values returned by getStatus
    STATUS_UNKNOWN,
    STATUS_FAILED_CONNECT,
    STATUS_FAILED_CONNECT_TIMEOUT,
    STATUS_SUCCESS
  };

  CellConnector();
  void connect(uint32_t timeout, void (*success)(uint32_t,int), void (*failure)(uint32_t,int), void (*finished)(uint32_t,bool));
  int getState();
  int getStatus();
  void abort();
  void loop(); // Need to call this in the loop function.

private:
  // How long to wait before considering a connect to have failed
  uint32_t connectTimeout;
  uint32_t lastStateTime, startConnectTime;
  int failureCount;
  int state;
  int status;
  void (*successCallback)(uint32_t,int);
  void (*failureCalback)(uint32_t,int);
  void (*finishedCallback)(uint32_t,bool);
};
